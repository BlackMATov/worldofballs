-module(wob_physics).

-export([create_world/0]).
-export([update_world/1]).
-export([contacts_world/1]).
-export([destroy_world/1]).
-export([create_box/4]).
-export([create_circle/4]).
-export([destroy_body/1]).
-export([replace_circle/2]).
-export([apply_force/2]).
-export([apply_impulse/2]).
-export([get_body_id/1]).
-export([get_body_pos/1]).
-export([get_body_lvel/1]).
-export([set_body_lvel/2]).
-export([get_body_world/1]).
-on_load(init/0).

-include("../include/types.hrl").

%% ------------------------------------
%% init
%% ------------------------------------

init() ->
	ok = erlang:load_nif("priv/wob_physics", 0).

%% ------------------------------------
%% create_world
%% ------------------------------------

-spec create_world(
) -> integer().

create_world() ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% update_world
%% ------------------------------------

-spec update_world(
	integer()
) -> integer().

update_world(_World) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% contacts_world
%% ------------------------------------

-spec contacts_world(
	integer()
) -> list().

contacts_world(_World) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% destroy_world
%% ------------------------------------

-spec destroy_world(
	integer()
) -> integer().

destroy_world(_World) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% create_box
%% ------------------------------------

-spec create_box(
	integer(), vec2(), vec2(), integer()
) -> integer().

create_box(_World, _Pos, _Size, _Id) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% create_circle
%% ------------------------------------

-spec create_circle(
	integer(), vec2(), float(), integer()
) -> integer().

create_circle(_World, _Pos, _R, _Id) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% destroy_body
%% ------------------------------------

-spec destroy_body(
	integer()
) -> integer().

destroy_body(_Body) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% replace_circle
%% ------------------------------------

-spec replace_circle(
	integer(), float()
) -> integer().

replace_circle(_Body, _R) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% apply_force
%% ------------------------------------

-spec apply_force(
	integer(), vec2()
) -> integer().

apply_force(_Body, _Force) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% apply_impulse
%% ------------------------------------

-spec apply_impulse(
	integer(), vec2()
) -> integer().

apply_impulse(_Body, _Impulse) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% get_body_id
%% ------------------------------------

-spec get_body_id(
	integer()
) -> integer().

get_body_id(_Body) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% get_body_pos
%% ------------------------------------

-spec get_body_pos(
	integer()
) -> vec2().

get_body_pos(_Body) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% get_body_lvel
%% ------------------------------------

-spec get_body_lvel(
	integer()
) -> vec2().

get_body_lvel(_Body) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% set_body_lvel
%% ------------------------------------

-spec set_body_lvel(
	integer(), vec2()
) -> vec2().

set_body_lvel(_Body, _LVel) ->
	exit(nif_library_not_loaded).

%% ------------------------------------
%% get_body_world
%% ------------------------------------

-spec get_body_world(
	integer()
) -> integer().

get_body_world(_Body) ->
	exit(nif_library_not_loaded).


