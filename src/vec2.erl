-module(vec2).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

-export([new/0, new/1, new/2]).
-export([one/0, zero/0, one_zero/0, zero_one/0]).
-export([neg/1]).
-export([len/1]).
-export([add/2]).
-export([sub/2]).
-export([mul/2]).
-export([divide/2]).
-export([distance/2]).
-export([normalize/1]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% new/0
%% ------------------------------------

-spec new() -> vec2().

new() ->
	{0.0, 0.0}.

%% ------------------------------------
%% new/1
%% ------------------------------------

-spec new(number()) -> vec2().

new(V)
when is_number(V) ->
	{V / 1.0, V / 1.0}.

%% ------------------------------------
%% new/2
%% ------------------------------------

-spec new(number(), number()) -> vec2().

new(X, Y)
when is_number(X), is_number(Y) ->
	{X / 1.0, Y / 1.0}.

%% ------------------------------------
%% predefs
%% ------------------------------------

-spec one() -> vec2().

one() ->
	{1.0, 1.0}.

-spec zero() -> vec2().

zero() ->
	{0.0, 0.0}.

-spec one_zero() -> vec2().

one_zero() ->
	{1.0, 0.0}.

-spec zero_one() -> vec2().

zero_one() ->
	{0.0, 1.0}.

%% ------------------------------------
%% neg
%% ------------------------------------

-spec neg(vec2()) -> vec2().

neg({X, Y})
when is_float(X), is_float(Y) ->
	{-X, -Y}.

%% ------------------------------------
%% len
%% ------------------------------------

-spec len(vec2()) -> float().

len({X, Y})
when is_float(X), is_float(Y) ->
	math:sqrt(X * X + Y * Y).

%% ------------------------------------
%% add
%% ------------------------------------

-spec add(
	vec2(),
	vec2() | number()
) -> vec2().

add({X, Y}, V)
when is_float(X), is_float(Y), is_number(V) ->
	{X + (V / 1.0), Y + (V / 1.0)};
add({X1, Y1}, {X2, Y2})
when is_float(X1), is_float(Y1), is_float(X2), is_float(Y2) ->
	{X1 + X2, Y1 + Y2}.

%% ------------------------------------
%% sub
%% ------------------------------------

-spec sub(
	vec2(),
	vec2() | number()
) -> vec2().

sub({X, Y}, V)
when is_float(X), is_float(Y), is_number(V) ->
	{X - (V / 1.0), Y - (V / 1.0)};
sub({X1, Y1}, {X2, Y2})
when is_float(X1), is_float(Y1), is_float(X2), is_float(Y2) ->
	{X1 - X2, Y1 - Y2}.

%% ------------------------------------
%% mul
%% ------------------------------------

-spec mul(
	vec2(),
	vec2() | number()
) -> vec2().

mul({X, Y}, V)
when is_float(X), is_float(Y), is_number(V) ->
	{X * (V / 1.0), Y * (V / 1.0)};
mul({X1, Y1}, {X2, Y2})
when is_float(X1), is_float(Y1), is_float(X2), is_float(Y2) ->
	{X1 * X2, Y1 * Y2}.

%% ------------------------------------
%% divide
%% ------------------------------------

-spec divide(
	vec2(),
	vec2() | number()
) -> vec2().

divide({X, Y}, V)
when is_float(X), is_float(Y), is_number(V) ->
	{X / (V / 1.0), Y / (V / 1.0)};
divide({X1, Y1}, {X2, Y2})
when is_float(X1), is_float(Y1), is_float(X2), is_float(Y2) ->
	{X1 / X2, Y1 / Y2}.

%% ------------------------------------
%% distance
%% ------------------------------------

-spec distance(vec2(), vec2()) -> float().

distance(V1 = {X1, Y1}, V2 = {X2, Y2})
when is_float(X1), is_float(Y1), is_float(X2), is_float(Y2) ->
	vec2:len(vec2:sub(V2, V1)).

%% ------------------------------------
%% normalize
%% ------------------------------------

-spec normalize(vec2()) -> vec2().

normalize(V = {X, Y})
when is_float(X), is_float(Y) ->
	Len = vec2:len(V),
	try {X/Len, Y/Len}
	catch error:badarith -> {0.0, 0.0} end.

%% ----------------------------------------------------------------------------
%%
%% TESTS
%%
%% ----------------------------------------------------------------------------

-include_lib("eunit/include/eunit.hrl").

new_test() ->
	{X1, Y1} = vec2:new(),
	{X2, Y2} = vec2:new(5.0),
	{X3, Y3} = vec2:new(1.0, 2.0),
	?assertEqual(X1, 0.0),
	?assertEqual(Y1, 0.0),
	?assertEqual(X2, 5.0),
	?assertEqual(Y2, 5.0),
	?assertEqual(X3, 1.0),
	?assertEqual(Y3, 2.0).

predefs_test() ->
	{X1, Y1} = vec2:one(),
	{X2, Y2} = vec2:zero(),
	{X3, Y3} = vec2:one_zero(),
	{X4, Y4} = vec2:zero_one(),
	?assertEqual(X1, 1.0),
	?assertEqual(Y1, 1.0),
	?assertEqual(X2, 0.0),
	?assertEqual(Y2, 0.0),
	?assertEqual(X3, 1.0),
	?assertEqual(Y3, 0.0),
	?assertEqual(X4, 0.0),
	?assertEqual(Y4, 1.0).

neg_test() ->
	{X1, Y1} = vec2:neg(vec2:new( 1.0,  2.0)),
	{X2, Y2} = vec2:neg(vec2:new(-1.0,  2.0)),
	{X3, Y3} = vec2:neg(vec2:new(-3.0, -4.0)),
	?assertEqual(X1, -1.0),
	?assertEqual(Y1, -2.0),
	?assertEqual(X2,  1.0),
	?assertEqual(Y2, -2.0),
	?assertEqual(X3,  3.0),
	?assertEqual(Y3,  4.0).

len_test() ->
	L1 = vec2:len(vec2:new( 1.0,  0.0)),
	L2 = vec2:len(vec2:new( 0.0,  1.0)),
	L3 = vec2:len(vec2:new(-1.0,  0.0)),
	L4 = vec2:len(vec2:new( 0.0, -1.0)),
	L5 = vec2:len(vec2:new( 0.0,  0.0)),
	?assertEqual(L1, 1.0),
	?assertEqual(L2, 1.0),
	?assertEqual(L3, 1.0),
	?assertEqual(L4, 1.0),
	?assertEqual(L5, 0.0).

add1_test() ->
	{X1, Y1} = vec2:add(vec2:new( 1.0, -2.0),  3.0),
	{X2, Y2} = vec2:add(vec2:new(-3.0,  4.0), -3.0),
	?assertEqual(X1,   4.0),
	?assertEqual(Y1,   1.0),
	?assertEqual(X2,  -6.0),
	?assertEqual(Y2,   1.0).

add2_test() ->
	{X1, Y1} = vec2:add(vec2:new( 1.0,  2.0), vec2:new( 3.0,  4.0)),
	{X2, Y2} = vec2:add(vec2:new(-3.0, -4.0), vec2:new( 3.0,  4.0)),
	{X3, Y3} = vec2:add(vec2:new(-3.0, -4.0), vec2:new(-3.0, -4.0)),
	?assertEqual(X1,  4.0),
	?assertEqual(Y1,  6.0),
	?assertEqual(X2,  0.0),
	?assertEqual(Y2,  0.0),
	?assertEqual(X3, -6.0),
	?assertEqual(Y3, -8.0).

sub1_test() ->
	{X1, Y1} = vec2:sub(vec2:new( 1.0, -2.0),  3.0),
	{X2, Y2} = vec2:sub(vec2:new(-3.0,  4.0), -3.0),
	?assertEqual(X1,  -2.0),
	?assertEqual(Y1,  -5.0),
	?assertEqual(X2,   0.0),
	?assertEqual(Y2,   7.0).

sub2_test() ->
	{X1, Y1} = vec2:sub(vec2:new(10.0, 20.0), vec2:new( 3.0,  4.0)),
	{X2, Y2} = vec2:sub(vec2:new(-3.0, -4.0), vec2:new( 3.0,  4.0)),
	{X3, Y3} = vec2:sub(vec2:new(-3.0, -4.0), vec2:new(-3.0, -4.0)),
	?assertEqual(X1,  7.0),
	?assertEqual(Y1, 16.0),
	?assertEqual(X2, -6.0),
	?assertEqual(Y2, -8.0),
	?assertEqual(X3,  0.0),
	?assertEqual(Y3,  0.0).

mul1_test() ->
	{X1, Y1} = vec2:mul(vec2:new( 1.0, -2.0),  3.0),
	{X2, Y2} = vec2:mul(vec2:new(-3.0,  4.0), -3.0),
	?assertEqual(X1,   3.0),
	?assertEqual(Y1,  -6.0),
	?assertEqual(X2,   9.0),
	?assertEqual(Y2, -12.0).

mul2_test() ->
	{X1, Y1} = vec2:mul(vec2:new(10.0, 20.0), vec2:new( 3.0,  4.0)),
	{X2, Y2} = vec2:mul(vec2:new(-3.0, -4.0), vec2:new( 3.0,  4.0)),
	{X3, Y3} = vec2:mul(vec2:new(-3.0, -4.0), vec2:new(-3.0, -4.0)),
	?assertEqual(X1,  30.0),
	?assertEqual(Y1,  80.0),
	?assertEqual(X2,  -9.0),
	?assertEqual(Y2, -16.0),
	?assertEqual(X3,   9.0),
	?assertEqual(Y3,  16.0).

divide1_test() ->
	{X1, Y1} = vec2:divide(vec2:new( 6.0, -3.0),  3.0),
	{X2, Y2} = vec2:divide(vec2:new(-3.0,  9.0), -3.0),
	?assertEqual(X1,   2.0),
	?assertEqual(Y1,  -1.0),
	?assertEqual(X2,   1.0),
	?assertEqual(Y2,  -3.0).

divide2_test() ->
	{X1, Y1} = vec2:divide(vec2:new( 9.0, 20.0), vec2:new( 3.0,  4.0)),
	{X2, Y2} = vec2:divide(vec2:new(-3.0, -4.0), vec2:new( 3.0,  4.0)),
	{X3, Y3} = vec2:divide(vec2:new(-3.0, -4.0), vec2:new(-3.0, -4.0)),
	?assertEqual(X1,  3.0),
	?assertEqual(Y1,  5.0),
	?assertEqual(X2, -1.0),
	?assertEqual(Y2, -1.0),
	?assertEqual(X3,  1.0),
	?assertEqual(Y3,  1.0).

normalize_test() ->
	{X1, Y1} = vec2:normalize(vec2:new( 2.0, 0.0)),
	{X2, Y2} = vec2:normalize(vec2:new( 0.0, 3.0)),
	{X3, Y3} = vec2:normalize(vec2:new(-1.0, 0.0)),
	{X4, Y4} = vec2:normalize(vec2:new( 0.0, 0.0)),
	?assertEqual(X1,  1.0),
	?assertEqual(Y1,  0.0),
	?assertEqual(X2,  0.0),
	?assertEqual(Y2,  1.0),
	?assertEqual(X3, -1.0),
	?assertEqual(Y3,  0.0),
	?assertEqual(X4,  0.0),
	?assertEqual(Y4,  0.0).
