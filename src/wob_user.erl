-module(wob_user).

-behaviour(gen_server).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([start_link/1]).
-export([stop/1]).

-export([get_idir/1]).
-export([absorb_handle/2]).
-export([unabsorb_handle/1]).
-export([incoming_message/3]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
	pid()
) -> {ok, pid()}.

start_link(WSPid) ->
	gen_server:start_link(?MODULE, [WSPid], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
	pid()
) -> ok.

stop(Pid) ->
	gen_server:call(Pid, stop).

%% ------------------------------------
%% get_idir
%% ------------------------------------

-spec get_idir(
	pid()
) -> vec2().

get_idir(Pid) ->
	gen_server:call(Pid, get_idir).

%% ------------------------------------
%% absorb_handle
%% ------------------------------------

-spec absorb_handle(
	pid(), integer()
) -> boolean().

absorb_handle(Pid, EntityId) ->
	gen_server:call(Pid, {absorb_handle, EntityId}).

%% ------------------------------------
%% unabsorb_handle
%% ------------------------------------

-spec unabsorb_handle(
	pid()
) -> ok.

unabsorb_handle(Pid) ->
	gen_server:call(Pid, unabsorb_handle).

%% ------------------------------------
%% incoming_message
%% ------------------------------------

-spec incoming_message(
	pid(), binary(), json()
) -> ok.

incoming_message(Pid, MsgId, MsgValue) ->
	gen_server:cast(Pid, {incoming_message, MsgId, MsgValue}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init([WSPid]) ->
	State = #user_state2{
		pid   = self(),
		wspid = WSPid
	},
	wob_loginer:remember_user(self(), WSPid),
	{ok, State}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(get_idir, _From, State) ->
	IDir = State#user_state2.idir,
	{reply, IDir, State};

handle_call({absorb_handle, EntityId}, _From, State) ->
	{Result, NewState} = case State#user_state2.entity of
		-1 ->
			wob_stats:log("wob_user:absorb_handle:~p~n", [EntityId]),
			{true, State#user_state2{entity = EntityId}};
		_ ->
			{false, State}
	end,
	{reply, Result, NewState};

handle_call(unabsorb_handle, _From, State) ->
	OldEntityId = State#user_state2.entity,
	wob_stats:log("wob_user:unabsorb_handle:~p~n", [OldEntityId]),
	NewState = State#user_state2{entity = -1},
	{reply, ok, NewState};

handle_call(stop, _From, State) ->
	NewState = stop_impl(State),
	{stop, normal, ok, NewState};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({incoming_message, MsgId, MsgValue}, State) ->
	NewState = incoming_message_impl(MsgId, MsgValue, State),
	{noreply, NewState};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(_Reason, _State) ->
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% stop_impl
%% ------------------------------------

-spec stop_impl(
	user_state2()
) -> user_state2().

stop_impl(State) ->
	EntityId = State#user_state2.entity,
	wob_game:unabsorb_entity(EntityId),
	wob_loginer:forget_user(self()),
	State.

%% ------------------------------------
%% incoming_message_impl
%% ------------------------------------

-spec incoming_message_impl(
	binary(), json(), user_state2()
) -> user_state2().

incoming_message_impl(<<"request_ping">>, MsgValue, State) ->
	WSPid = State#user_state2.wspid,
	webserver_ws:send_json(WSPid, <<"ping_info">>, MsgValue),
	State;

incoming_message_impl(<<"force">>, MsgValue, State) ->
	{[{<<"x">>, X},{<<"y">>, Y}]} = MsgValue,
	FPoint = vec2:new(X,Y),
	case State#user_state2.entity of
		-1 ->
			UserPid = State#user_state2.pid,
			wob_game:absorb_entity(UserPid, FPoint),
			State;
		_ ->
			State#user_state2{idir = FPoint}
	end;

incoming_message_impl(<<"unforce">>, _MsgValue, State) ->
	State#user_state2{idir = vec2:zero()};

incoming_message_impl(_MsgId, _MsgValue, State) ->
	State.
