-module(wob_logic).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([create_food_brain/0]).
-export([create_user_brain/1]).
-export([flush_brain/1]).
-export([think/2]).
-export([is_dead/1]).
-export([collision/2]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% create_food_brain
%% ------------------------------------

-spec create_food_brain(
) -> brain_desc().

create_food_brain() ->
	{food, #food_brain{}}.

%% ------------------------------------
%% create_user_brain
%% ------------------------------------

-spec create_user_brain(
	pid()
) -> brain_desc().

create_user_brain(UserPid) ->
	{user, #user_brain{
		pid = UserPid
	}}.

%% ------------------------------------
%% flush_brain
%% ------------------------------------

-spec flush_brain(
	brain_desc()
) -> brain_desc().

flush_brain({user, UserBrain}) ->
	UserPid = UserBrain#user_brain.pid,
	wob_user:unabsorb_handle(UserPid),
	create_food_brain();

flush_brain(_Brain) ->
	create_food_brain().

%% ------------------------------------
%% think
%% ------------------------------------

-spec think(
	entity_state(), pid()
) -> entity_state().

think(Entity = #entity_state{brain = {food, _FoodBrain}}, _World) ->
	R        = Entity#entity_state.r,
	Body     = Entity#entity_state.body,
	MaxForce = max_force_from_radius(R),
	NewBody  = wob_physics:apply_force(Body, vec2:new(
		MaxForce * (random:uniform() * 1.0 - 0.5),
		MaxForce * (random:uniform() * 1.0 - 0.5)
	)),
	Entity#entity_state{body = NewBody};

think(Entity = #entity_state{brain = {user, UserBrain}}, World) ->
	UserPid  = UserBrain#user_brain.pid,
	LostArea = UserBrain#user_brain.lost_area,
	IDir     = wob_user:get_idir(UserPid),
	IDirLen  = vec2:len(IDir),
	if
		IDirLen > 0.0 ->
			R            = Entity#entity_state.r,
			Pos          = Entity#entity_state.pos,
			Body         = Entity#entity_state.body,
			MaxForce     = max_force_from_radius(R),
			DirForce     = vec2:mul(vec2:normalize(vec2:sub(Pos, IDir)), MaxForce),
			NewR         = R - R * 0.003,
			NewBody      = wob_physics:replace_circle(Body, NewR),
			NewUserBrain = UserBrain#user_brain{
				lost_area = LostArea + (geom:circle_area(R) - geom:circle_area(NewR))
			},
			NewEntity = Entity#entity_state{
				r     = NewR,
				body  = wob_physics:apply_force(NewBody, DirForce),
				brain = {user, NewUserBrain}
			},
			emit_lost_food(NewEntity, World, DirForce);
		true ->
			Entity
	end;

think(Entity, _World) ->
	Entity.

%% ------------------------------------
%% is_dead
%% ------------------------------------

-spec is_dead(
	entity_state()
) -> boolean().

is_dead(Entity) ->
	R = Entity#entity_state.r,
	R < ?WORLD_MIN_LIVE_SIZE.

%% ------------------------------------
%% collision
%% ------------------------------------

-spec collision(
	entity_state(), entity_state()
) -> {entity_state(), entity_state()}.

collision(EntityA, EntityB) ->
	RA   = EntityA#entity_state.r,
	RB   = EntityB#entity_state.r,
	PosA = EntityA#entity_state.pos,
	PosB = EntityB#entity_state.pos,
	{MinEntity, MinR, MaxEntity, MaxR} = if
		RA < RB -> {EntityA, RA, EntityB, RB};
		true    -> {EntityB, RB, EntityA, RA}
	end,
	MinA  = geom:circle_area(MinR),
	MaxA  = geom:circle_area(MaxR),
	DDist = (RA + RB) - vec2:distance(PosA, PosB),
	if
		DDist > 0.0 ->
			NewMinR      = erlang:max(MinR - DDist, 0.0),
			NewMaxR      = geom:circle_radius(MaxA + (MinA - geom:circle_area(NewMinR))),
			NewMinBody   = wob_physics:replace_circle(MinEntity#entity_state.body, NewMinR),
			NewMaxBody   = wob_physics:replace_circle(MaxEntity#entity_state.body, NewMaxR),
			NewMinEntity = MinEntity#entity_state{r = NewMinR, body = NewMinBody},
			NewMaxEntity = MaxEntity#entity_state{r = NewMaxR, body = NewMaxBody},
			if
				RA < RB -> {NewMinEntity, NewMaxEntity};
				true    -> {NewMaxEntity, NewMinEntity}
			end;
		true ->
			{EntityA, EntityB}
	end.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% emit_lost_food
%% ------------------------------------

-spec emit_lost_food(
	entity_state(), pid(), vec2()
) -> entity_state().

emit_lost_food(Entity = #entity_state{brain = {user, UserBrain}}, World, DirForce) ->
	R     = Entity#entity_state.r,
	Pos   = Entity#entity_state.pos,
	LostR = geom:circle_radius(UserBrain#user_brain.lost_area),
	NewLostArea = if
		((LostR > R * 0.3) or (LostR > ?WORLD_MAX_EMIT_SIZE)) and (LostR > ?WORLD_MIN_LIVE_SIZE) ->
			Dir     = vec2:normalize(DirForce),
			FoodPos = vec2:add(Pos, vec2:mul(Dir, - R - LostR - ?WORLD_MIN_LIVE_SIZE)),
			wob_world:create_food(World, FoodPos, LostR, fun(FoodEntity) ->
				FoodBody = FoodEntity#entity_state.body,
				NewFoodBody = wob_physics:apply_impulse(FoodBody, vec2:mul(vec2:neg(DirForce), 0.2)),
				FoodEntity#entity_state{body = NewFoodBody}
			end),
			0.0;
		true ->
			geom:circle_area(LostR)
	end,
	NewUserBrain = UserBrain#user_brain{
		lost_area = NewLostArea
	},
	Entity#entity_state{
		brain = {user, NewUserBrain}
	}.

%% ------------------------------------
%% max_force_from_radius
%% ------------------------------------

-spec max_force_from_radius(
	float()
) -> float().

max_force_from_radius(R) ->
	R * 0.5.
