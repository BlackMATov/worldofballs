-module(wob_game).
-behaviour(gen_server).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([start_link/0]).
-export([stop/0]).

-export([absorb_entity/2]).
-export([unabsorb_entity/1]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	gen_server:call(?MODULE, stop).

%% ------------------------------------
%% absorb_entity
%% ------------------------------------

-spec absorb_entity(
	pid(), vec2()
) -> ok.

absorb_entity(UserPid, Pos) ->
	gen_server:cast(?MODULE, {absorb_entity, UserPid, Pos}).

%% ------------------------------------
%% unabsorb_entity
%% ------------------------------------

-spec unabsorb_entity(
	integer()
) -> ok.

unabsorb_entity(EntityId) ->
	gen_server:call(?MODULE, {unabsorb_entity, EntityId}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init([]) ->
	erlang:send_after(?WORLD_RECREATE_INTERVAL, self(), recreate_world),
	{ok, World} = wob_world:start_link(?WORLD_FOOD_COUNT),
	{ok, #game_state{world = World}}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	NewState = stop_impl(State),
	{stop, normal, ok, NewState};

handle_call({unabsorb_entity, EntityId}, _From, State) ->
	World = State#game_state.world,
	wob_world:unabsorb_entity(World, EntityId),
	{reply, ok, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({absorb_entity, UserPid, Pos}, State) ->
	World = State#game_state.world,
	wob_world:absorb_entity(World, UserPid, Pos),
	{noreply, State};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(recreate_world, State) ->
	NewState = recreate_world_impl(State),
	erlang:send_after(?WORLD_RECREATE_INTERVAL, self(), recreate_world),
	{noreply, NewState};

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(_Reason, _State) ->
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% stop_impl
%% ------------------------------------

-spec stop_impl(
	game_state()
) -> game_state().

stop_impl(State) ->
	wob_stats:log("wob_game:stop_impl~n"),
	wob_world:stop(State#game_state.world),
	State#game_state{world = undefined}.

%% ------------------------------------
%% recreate_world_impl
%% ------------------------------------

-spec recreate_world_impl(
	game_state()
) -> game_state().

recreate_world_impl(State) ->
	wob_stats:log("wob_game:recreate_world_impl~n"),
	wob_world:stop(State#game_state.world),
	{ok, NewWorld} = wob_world:start_link(?WORLD_FOOD_COUNT),
	State#game_state{world = NewWorld}.
