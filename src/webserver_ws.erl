-module(webserver_ws).

-behaviour(cowboy_websocket_handler).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([send_json/2]).
-export([send_json/3]).
-export([send_text/2]).

%% cowboy_websocket_handler
-export([init/3]).
-export([websocket_init/3]).
-export([websocket_handle/3]).
-export([websocket_info/3]).
-export([websocket_terminate/3]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% send_json/2
%% ------------------------------------

-spec send_json(
	pid(), json()
) -> ok.

send_json(WSPid, JsonMsg) ->
	WSPid ! {send_json, JsonMsg},
	ok.

%% ------------------------------------
%% send_json/3
%% ------------------------------------

-spec send_json(
	pid(), binary(), json()
) -> ok.

send_json(WSPid, MsgId, MsgValue) ->
	JsonMsg = {[
		{<<"id">>,    MsgId},
		{<<"value">>, MsgValue}
	]},
	send_json(WSPid, JsonMsg).

%% ------------------------------------
%% send_text
%% ------------------------------------

-spec send_text(
	pid(), binary()
) -> ok.

send_text(WSPid, Msg) ->
	WSPid ! {send_text, Msg},
	ok.

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR cowboy_websocket_handler
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init({tcp, http}, _Req, _Opts) ->
	{upgrade, protocol, cowboy_websocket}.

%% ------------------------------------
%% websocket_init
%% ------------------------------------

websocket_init(_TransportName, Req, _Opts) ->
	{ok, UserPid} = wob_user:start_link(self()),
	{ok, Req, UserPid}.

%% ------------------------------------
%% websocket_handle
%% ------------------------------------

websocket_handle({text, Msg}, Req, State) ->
	json_handle(jiffy:decode(Msg), State),
	{ok, Req, State};

websocket_handle(_Data, Req, State) ->
	{ok, Req, State}.

%% ------------------------------------
%% websocket_info
%% ------------------------------------

websocket_info({send_json, Json}, Req, State) ->
	Info = {text, jiffy:encode(Json)},
	{reply, Info, Req, State};

websocket_info({send_text, Msg}, Req, State) ->
	Info = {text, Msg},
	{reply, Info, Req, State};

websocket_info(_Info, Req, State) ->
	{ok, Req, State}.

%% ------------------------------------
%% websocket_terminate
%% ------------------------------------

websocket_terminate(_Reason, _Req, State) ->
	wob_user:stop(State).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% json_handle
%% ------------------------------------

-spec json_handle(
	json(), pid()
) -> ok.

json_handle({[{<<"id">>, MsgId}, {<<"value">>, MsgValue}]}, State) ->
	UserPid = State,
	wob_user:incoming_message(UserPid, MsgId, MsgValue);

json_handle(_Json, State) ->
	State.
