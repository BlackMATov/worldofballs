-module(wob_stats).
-behaviour(gen_server).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([start_link/0]).
-export([stop/0]).

-export([log/1,log/2]).
-export([perf/2]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	gen_server:call(?MODULE, stop).

%% ------------------------------------
%% log/1
%% ------------------------------------

-spec log(
	string()
) -> ok.

log(Text) ->
	gen_server:cast(?MODULE, {log, Text, []}).

%% ------------------------------------
%% log/2
%% ------------------------------------

-spec log(
	string(), list()
) -> ok.

log(Text, Args) ->
	gen_server:cast(?MODULE, {log, Text, Args}).

%% ------------------------------------
%% perf
%% ------------------------------------

-spec perf(
	binary(), integer()
) -> ok.

perf(Name, Time) ->
	gen_server:cast(?MODULE, {perf, Name, Time}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init([]) ->
	State = #stats_state{},
	{ok, State}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	io:format("wob_stats:stop~n"),
	{stop, normal, ok, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({log, Text, Args}, State) ->
	io:format(Text, Args),
	{noreply, State};
handle_cast({perf, Name, Time}, State) ->
	io:format("Report: ~p(~p)~n", [Name, Time/1000.0]),
	{noreply, State};
handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(_Reason, _State) ->
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------
