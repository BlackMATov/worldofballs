-module(wob_loginer).
-behaviour(gen_server).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([start_link/0]).
-export([stop/0]).

-export([forget_user/1]).
-export([remember_user/2]).
-export([send_json_to_all/1]).
-export([send_json_to_all/2]).
-export([send_text_to_all/1]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
) -> {ok, pid()}.

start_link() ->
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
) -> ok.

stop() ->
	gen_server:call(?MODULE, stop).

%% ------------------------------------
%% forget_user
%% ------------------------------------

-spec forget_user(
	pid()
) -> ok.

forget_user(UserPid) ->
	gen_server:cast(?MODULE, {forget_user, UserPid}).

%% ------------------------------------
%% remember_user
%% ------------------------------------

-spec remember_user(
	pid(), pid()
) -> ok.

remember_user(UserPid, UserWSPid) ->
	gen_server:cast(?MODULE, {remember_user, UserPid, UserWSPid}).

%% ------------------------------------
%% send_json_to_all/1
%% ------------------------------------

-spec send_json_to_all(
	json()
) -> ok.

send_json_to_all(JsonMsg) ->
	gen_server:cast(?MODULE, {send_json_to_all, JsonMsg}).

%% ------------------------------------
%% send_json_to_all/2
%% ------------------------------------

-spec send_json_to_all(
	binary(), json()
) -> ok.

send_json_to_all(MsgId, MsgValue) ->
	gen_server:cast(?MODULE, {send_json_to_all, MsgId, MsgValue}).

%% ------------------------------------
%% send_text_to_all
%% ------------------------------------

-spec send_text_to_all(
	binary()
) -> ok.

send_text_to_all(Msg) ->
	gen_server:cast(?MODULE, {send_text_to_all, Msg}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init([]) ->
	State = #loginer_state{},
	{ok, State}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	wob_stats:log("wob_loginer:stop~n"),
	{stop, normal, ok, State};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({forget_user, UserPid}, State) ->
	Users    = State#loginer_state.users,
	NewUsers = dict:erase(UserPid, Users),
	NewState = State#loginer_state{
		users = NewUsers
	},
	wob_stats:log("wob_loginer:forget_user(user_pid:~p, count:~p)~n",
		[UserPid, dict:size(NewUsers)]),
	{noreply, NewState};

handle_cast({remember_user, UserPid, UserWSPid}, State) ->
	Users    = State#loginer_state.users,
	NewUsers = dict:store(UserPid, UserWSPid, Users),
	NewState = State#loginer_state{
		users = NewUsers
	},
	wob_stats:log("wob_loginer:remember_user(user_pid:~p, count:~p)~n",
		[UserPid, dict:size(NewUsers)]),
	{noreply, NewState};

handle_cast({send_json_to_all, JsonMsg}, State) ->
	dict:map(fun(_UserPid, UserWSPid) ->
		webserver_ws:send_json(UserWSPid, JsonMsg)
	end, State#loginer_state.users),
	{noreply, State};

handle_cast({send_json_to_all, MsgId, MsgValue}, State) ->
	dict:map(fun(_UserPid, UserWSPid) ->
		webserver_ws:send_json(UserWSPid, MsgId, MsgValue)
	end, State#loginer_state.users),
	{noreply, State};

handle_cast({send_text_to_all, Msg}, State) ->
	dict:map(fun(_UserPid, UserWSPid) ->
		webserver_ws:send_text(UserWSPid, Msg)
	end, State#loginer_state.users),
	{noreply, State};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(_Reason, _State) ->
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------
