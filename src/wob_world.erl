-module(wob_world).
-behaviour(gen_server).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS and INCLUDES
%%
%% ----------------------------------------------------------------------------

%% public
-export([start_link/1]).
-export([stop/1]).

-export([create_food/3]).
-export([create_food/4]).
-export([absorb_entity/3]).
-export([unabsorb_entity/2]).

%% gen_server
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-include("../include/types.hrl").

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start_link
%% ------------------------------------

-spec start_link(
	integer()
) -> {ok, pid()}.

start_link(FoodCount) ->
	gen_server:start_link(?MODULE, [FoodCount], []).

%% ------------------------------------
%% stop
%% ------------------------------------

-spec stop(
	pid()
) -> ok.

stop(Pid) ->
	gen_server:call(Pid, stop).

%% ------------------------------------
%% create_food/3
%% ------------------------------------

-spec create_food(
	pid(), vec2(), float()
) -> ok.

create_food(Pid, Pos, R) ->
	gen_server:cast(Pid, {create_food, Pos, R}).

%% ------------------------------------
%% create_food/4
%% ------------------------------------

-spec create_food(
	pid(), vec2(), float(), fun()
) -> ok.

create_food(Pid, Pos, R, Func) ->
	gen_server:cast(Pid, {create_food, Pos, R, Func}).

%% ------------------------------------
%% absorb_entity
%% ------------------------------------

-spec absorb_entity(
	pid(), pid(), vec2()
) -> ok.

absorb_entity(Pid, UserPid, Pos) ->
	gen_server:cast(Pid, {absorb_entity, UserPid, Pos}).

%% ------------------------------------
%% unabsorb_entity
%% ------------------------------------

-spec unabsorb_entity(
	pid(), integer()
) -> ok.

unabsorb_entity(Pid, EntityId) ->
	gen_server:call(Pid, {unabsorb_entity, EntityId}).

%% ----------------------------------------------------------------------------
%%
%% BEHAVIOUR gen_server
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% init
%% ------------------------------------

init([FoodCount]) ->
	erlang:send_after(?WORLD_REFRESH_INTERVAL,   self(), refresh),
	erlang:send_after(?WORLD_BROADCAST_INTERVAL, self(), broadcast),
	BoxState  = create_boxes(#world_state{pworld = wob_physics:create_world()}),
	FoodState = create_foods(FoodCount, BoxState),
	{ok, FoodState}.

%% ------------------------------------
%% handle_call
%% ------------------------------------

handle_call(stop, _From, State) ->
	NewState = stop_impl(State),
	{stop, normal, ok, NewState};

handle_call({unabsorb_entity, EntityId}, _From, State) ->
	NewState = unabsorb_entity_impl(EntityId, State),
	{reply, ok, NewState};

handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%% ------------------------------------
%% handle_cast
%% ------------------------------------

handle_cast({create_food, Pos, R}, State) ->
	{_Entity, NewState} = create_food_impl(Pos, R, State),
	{noreply, NewState};

handle_cast({create_food, Pos, R, Func}, State) ->
	{_Entity, NewState} = create_food_impl(Pos, R, Func, State),
	{noreply, NewState};

handle_cast({absorb_entity, UserPid, Pos}, State) ->
	NewState = absorb_entity_impl(UserPid, Pos, State),
	{noreply, NewState};

handle_cast(_Msg, State) ->
	{noreply, State}.

%% ------------------------------------
%% handle_info
%% ------------------------------------

handle_info(refresh, State) ->
	NewState = refresh_impl(State),
	erlang:send_after(?WORLD_REFRESH_INTERVAL, self(), refresh),
	{noreply, NewState};

handle_info(broadcast, State) ->
	NewState = broadcast_impl(State),
	erlang:send_after(?WORLD_BROADCAST_INTERVAL, self(), broadcast),
	{noreply, NewState};

handle_info(_Info, State) ->
	{noreply, State}.

%% ------------------------------------
%% terminate
%% ------------------------------------

terminate(_Reason, _State) ->
	ok.

%% ------------------------------------
%% code_change
%% ------------------------------------

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% stop_impl
%% ------------------------------------

-spec stop_impl(
	world_state()
) -> world_state().

stop_impl(State) ->
	Entities    = State#world_state.entities,
	NewEntities = dict:map(fun(_Id, Entity) ->
		destroy_entity(Entity)
	end, Entities),
	PWorld    = State#world_state.pworld,
	NewPWorld = wob_physics:destroy_world(PWorld),
	State#world_state{
		pworld   = NewPWorld,
		entities = NewEntities
	}.

%% ------------------------------------
%% is_absorbable
%% ------------------------------------

-spec is_absorbable(
	entity_state()
) -> boolean().

is_absorbable(Entity) ->
	Brain = Entity#entity_state.brain,
	case Brain of
		{food, _} -> true;
		_ -> false
	end.

%% ------------------------------------
%% create_food_impl/3
%% ------------------------------------

-spec create_food_impl(
	vec2(), float(), world_state()
) -> {entity_state(), world_state()}.

create_food_impl(Pos, R, State) ->
	create_entity(
		Pos, R,
		wob_logic:create_food_brain(),
		State).

%% ------------------------------------
%% create_food_impl/4
%% ------------------------------------

-spec create_food_impl(
	vec2(), float(), fun(), world_state()
) -> {entity_state(), world_state()}.

create_food_impl(Pos, R, Func, State) ->
	create_entity(
		Pos, R,
		wob_logic:create_food_brain(),
		Func,
		State).

%% ------------------------------------
%% absorb_entity_impl
%% ------------------------------------

-spec absorb_entity_impl(
	pid(), vec2(), world_state()
) -> world_state().

absorb_entity_impl(UserPid, Pos, State) ->
	Entities     = State#world_state.entities,
	FoodEntities = dict:filter(fun(_Id, Entity) ->
		EntityR   = Entity#entity_state.r,
		EntityPos = Entity#entity_state.pos,
		is_absorbable(Entity) and (vec2:distance(Pos, EntityPos) < EntityR)
	end, Entities),
	absorb_entities(UserPid, dict:to_list(FoodEntities), State).

%% ------------------------------------
%% absorb_entities
%% ------------------------------------

-spec absorb_entities(
	pid(), list(), world_state()
) -> world_state().

absorb_entities(_UserPid, [], State) ->
	State;

absorb_entities(UserPid, [{EntityId, Entity} | _Entities], State) ->
	NewEntities = dict:update(EntityId, fun(_) ->
		case wob_user:absorb_handle(UserPid, EntityId) of
			true ->
				Entity#entity_state{
					brain = wob_logic:create_user_brain(UserPid)
				};
			_ ->
				Entity
		end
	end, State#world_state.entities),
	State#world_state{
		entities = NewEntities
	}.

%% ------------------------------------
%% unabsorb_entity_impl
%% ------------------------------------

-spec unabsorb_entity_impl(
	integer(), world_state()
) -> world_state().

unabsorb_entity_impl(EntityId, State) ->
	Entities = State#world_state.entities,
	NewEntities = case dict:find(EntityId, Entities) of
		{ok, Entity} ->
			dict:update(EntityId, fun(_) ->
				Entity#entity_state{
					brain = wob_logic:create_food_brain()
				}
			end, Entities);
		_ ->
			Entities
	end,
	State#world_state{
		entities = NewEntities
	}.

%% ------------------------------------
%% create_boxes
%% ------------------------------------

-spec create_boxes(
	world_state()
) -> world_state().

create_boxes(State) ->
	%PWorld = State#world_state.pworld,
	%BP1 = vec2:new(              0,              -50),
	%BP2 = vec2:new(              0, ?WORLD_HEIGHT+50),
	%BP3 = vec2:new(            -50,                0),
	%BP4 = vec2:new(?WORLD_WIDTH+50,                0),
	%wob_physics:create_box(PWorld, BP1, vec2:new(1024, 50), -1),
	%wob_physics:create_box(PWorld, BP2, vec2:new(1024, 50), -1),
	%wob_physics:create_box(PWorld, BP3, vec2:new(50, 1024), -1),
	%wob_physics:create_box(PWorld, BP4, vec2:new(50, 1024), -1),
	State.

%% ------------------------------------
%% create_foods
%% ------------------------------------

-spec create_foods(
	integer(), world_state()
) -> world_state().

create_foods(0, State) ->
	State;

create_foods(Count, State) ->
	MinSize = ?WORLD_FOOD_MIN_SIZE,
	MaxSize = ?WORLD_FOOD_MAX_SIZE,
	Pos = vec2:new(
		random:uniform() * ?WORLD_WIDTH,
		random:uniform() * ?WORLD_HEIGHT),
	R = random:uniform() * (MaxSize - MinSize) + MinSize,
	{_Entity, NewState} = create_food_impl(Pos, R, State),
	create_foods(Count - 1, NewState).

%% ------------------------------------
%% create_entity/4
%% ------------------------------------

-spec create_entity(
	vec2(), float(), brain_desc(), world_state()
) -> {entity_state(), world_state()}.

create_entity(Pos, R, BrainDesc, State) ->
	create_entity(Pos, R, BrainDesc, fun(Entity) ->
		Entity
	end, State).

%% ------------------------------------
%% create_entity/5
%% ------------------------------------

-spec create_entity(
	vec2(), float(), brain_desc(), fun(), world_state()
) -> {entity_state(), world_state()}.

create_entity(Pos, R, BrainDesc, Func, State) ->
	PWorld      = State#world_state.pworld,
	Entities    = State#world_state.entities,
	LastFreeId  = State#world_state.last_free_id,
	Body        = wob_physics:create_circle(PWorld, Pos, R, LastFreeId),
	EntityState = #entity_state{
		id    = LastFreeId,
		r     = R,
		pos   = Pos,
		body  = Body,
		brain = BrainDesc
	},
	NewEntityState = apply(Func, [EntityState]),
	NewEntities = dict:store(LastFreeId, NewEntityState, Entities),
	NewState = State#world_state{
		last_free_id = LastFreeId + 1,
		entities     = NewEntities
	},
	{NewEntityState, NewState}.

%% ------------------------------------
%% destroy_entity
%% ------------------------------------

-spec destroy_entity(
	entity_state()
) -> entity_state().

destroy_entity(Entity) ->
	Body     = Entity#entity_state.body,
	Brain    = Entity#entity_state.brain,
	NewBody  = wob_physics:destroy_body(Body),
	NewBrain = wob_logic:flush_brain(Brain),
	Entity#entity_state{
		body  = NewBody,
		brain = NewBrain
	}.

%% ------------------------------------
%% refresh_impl
%% ------------------------------------

-spec refresh_impl(
	world_state()
) -> world_state().

refresh_impl(State) ->
	NewState  = entities_refresh(State),
	PWorld    = NewState#world_state.pworld,
	NewPWorld = wob_physics:update_world(PWorld),
	collision_refresh(NewState#world_state{
		pworld = NewPWorld
	}).

%% ------------------------------------
%% entities_refresh
%% ------------------------------------

-spec entities_refresh(
	world_state()
) -> world_state().

entities_refresh(State) ->
	Entities    = State#world_state.entities,
	NewEntities = dict:map(fun(_Id, Entity) ->
		Body = Entity#entity_state.body,
		wob_logic:think(Entity#entity_state{
			pos = wob_physics:get_body_pos(Body)
		}, self())
	end, Entities),
	entities_funeral(State#world_state{
		entities = NewEntities
	}).

%% ------------------------------------
%% entities_funeral
%% ------------------------------------

-spec entities_funeral(
	world_state()
) -> world_state().

entities_funeral(State) ->
	Entities    = State#world_state.entities,
	NewEntities = dict:filter(fun(_,Entity) ->
		case wob_logic:is_dead(Entity) of
			true ->
				destroy_entity(Entity),
				false;
			_ ->
				true
		end
	end, Entities),
	State#world_state{entities = NewEntities}.

%% ------------------------------------
%% collision_refresh
%% ------------------------------------

-spec collision_refresh(
	world_state()
) -> world_state().

collision_refresh(State) ->
	PWorld   = State#world_state.pworld,
	Contacts = wob_physics:contacts_world(PWorld),
	lists:foldl(fun(Contact, StateAcc) ->
		{EntityAId, EntityBId}   = Contact,
		Entities                 = StateAcc#world_state.entities,
		case {dict:find(EntityAId, Entities), dict:find(EntityBId, Entities)} of
			{{ok, EntityA}, {ok, EntityB}} ->
				{NewEntityA, NewEntityB} = wob_logic:collision(EntityA, EntityB),
				change_entities([NewEntityA, NewEntityB], StateAcc);
			_ ->
				StateAcc
		end
	end, State, Contacts).

%% ------------------------------------
%% change_entities
%% ------------------------------------

-spec change_entities(
	list(), world_state()
) -> world_state().

change_entities([], State) ->
	State;

change_entities([NewEntity | NewEntities], State) ->
	Entities         = State#world_state.entities,
	NewEntityId      = NewEntity#entity_state.id,
	NewStateEntities = dict:update(NewEntityId, fun(_) ->
		NewEntity
	end, Entities),
	change_entities(NewEntities, State#world_state{
		entities = NewStateEntities
	}).

%% ------------------------------------
%% broadcast_impl
%% ------------------------------------

-spec broadcast_impl(
	world_state()
) -> world_state().

broadcast_impl(State) ->
	Info = broadcast_info(State),
	wob_loginer:send_text_to_all(Info),
	State.

%% ------------------------------------
%% broadcast_info
%% ------------------------------------

-spec broadcast_info(
	world_state()
) -> binary().

broadcast_info(State) ->
	EntityList = dict:fold(fun(_Key, Value, AccIn) ->
		{X, Y}    = Value#entity_state.pos,
		{Type, _} = Value#entity_state.brain,
		lists:append(AccIn, [{[
			{<<"x">>,  X},
			{<<"y">>,  Y},
			{<<"t">>,  atom_to_binary(Type, latin1)},
			{<<"r">>,  Value#entity_state.r},
			{<<"id">>, Value#entity_state.id}
		]}])
	end, [], State#world_state.entities),
	Json = {[
		{<<"id">>, <<"world_info">>},
		{<<"value">>, {[{
			<<"entities">>, EntityList
		}]}}
	]},
	jiffy:encode(Json).
