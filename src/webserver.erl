-module(webserver).

%% ----------------------------------------------------------------------------
%%
%% EXPORTS
%%
%% ----------------------------------------------------------------------------

-export([start/0]).
-export([stop/0]).
-export([reload/0]).

%% ----------------------------------------------------------------------------
%%
%% DEFINES
%%
%% ----------------------------------------------------------------------------

-define(GENS, [
	wob_stats, wob_game, wob_loginer]).

-define(APPS, [
	crypto, ranch, cowlib, cowboy,
	syntax_tools, compiler, goldrush, lager,
	webserver]).

%% ----------------------------------------------------------------------------
%%
%% PUBLIC
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% start
%% ------------------------------------

start() ->
	ok = ensure_started_apps(?APPS),
	ok = ensure_started_gens(?GENS).

%% ------------------------------------
%% stop
%% ------------------------------------

stop() ->
	stop_gens(lists:reverse(?GENS)),
	stop_apps(lists:reverse(?APPS)).

%% ------------------------------------
%% reload
%% ------------------------------------

reload() ->
	make:all([load]).

%% ----------------------------------------------------------------------------
%%
%% PRIVATE
%%
%% ----------------------------------------------------------------------------

%% ------------------------------------
%% ensure_started_gens
%% ------------------------------------

ensure_started_gens([]) -> ok;
ensure_started_gens([Gen | Gens]) ->
	case Gen:start_link() of
		{ok, _GenPid} -> ensure_started_gens(Gens);
		{error, {already_started, _GenPid}} -> ensure_started_gens(Gens)
	end.

%% ------------------------------------
%% stop_gens
%% ------------------------------------

stop_gens([]) -> ok;
stop_gens([Gen | Gens]) ->
	Gen:stop(),
	stop_gens(Gens).

%% ------------------------------------
%% ensure_started_apps
%% ------------------------------------

ensure_started_apps([]) -> ok;
ensure_started_apps([App | Apps]) ->
	case application:start(App) of
		ok -> ensure_started_apps(Apps);
		{error, {already_started, App}} -> ensure_started_apps(Apps)
	end.

%% ------------------------------------
%% stop_apps
%% ------------------------------------

stop_apps([]) -> ok;
stop_apps([App | Apps]) ->
	application:stop(App),
	stop_apps(Apps).
