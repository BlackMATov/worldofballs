all: compile start

compile:
	rebar compile

start:
	erl -pa ebin -pa deps/*/ebin -s webserver
