dialyzer --build_plt --apps^
    erts^
    kernel^
    stdlib^
    deps/cowboy^
    deps/cowlib^
    deps/goldrush^
    deps/jiffy^
    deps/lager^
    deps/ranch
