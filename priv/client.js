function Client() {
	this._socket         = undefined;
	this._online         = false;
	this._handlers       = [];
	this._sps            = 0;
	this._sps_count      = 0;
	this._mps            = 0;
	this._mps_count      = 0;
	this._stats_interval = null;
};

Client.prototype.start = function(start_func, finish_func) {
	this._socket = new WebSocket("ws://" + window.location.host + "/websocket");
	
	this._socket.onopen = function() {
		this._online = true;
		this._stats_interval = setInterval(function() {
			this._sps = this._sps_count;
			this._sps_count = 0;
			this._mps = this._mps_count;
			this._mps_count = 0;
		}.bind(this), 1000);
		if ( start_func ) {
			start_func();
		}
	}.bind(this);
	
	this._socket.onclose = function() {
		this._online = false;
		clearInterval(this._stats_interval);
		this._stats_interval = null;
		if ( finish_func ) {
			finish_func();
		}
	}.bind(this);
	
	this._socket.onmessage = function(evt) {
		var msg = null;
		try {
			msg = JSON.parse(evt.data);
		} catch ( e ) {
			console.error("client.onmessage parse error");
		}
		if ( msg ) {
			this._mps_count++;
			for ( var i = 0; i < this._handlers.length; ++i ) {
				var handler = this._handlers[i];
				if ( handler.id === "" || msg.id === handler.id ) {
					handler.func(msg);
					if ( handler.once ) {
						this._handlers.splice(i, 1);
						--i;
					}
				}
			}
		}
	}.bind(this);
};

Client.prototype.is_online = function() {
	return this._online;
};

Client.prototype.is_offline = function() {
	return !this._online;
};

Client.prototype.get_status = function() {
	return this.is_online() ? "online" : "offline";
};

Client.prototype.get_sps = function() {
	return this._sps;
};

Client.prototype.get_mps = function() {
	return this._mps;
};

Client.prototype.add_handler = function(msg_id, handler, once) {
	this._handlers.push({
		id   : msg_id,
		func : handler,
		once : once || false
	});
};

Client.prototype.send = function(msg_id, value) {
	var msg = {
		id    : msg_id,
		value : value || {}
	};
	var str = JSON.stringify(msg);
	this._socket.send(str);
	this._sps_count++;
};

Client.prototype.request = function(request, value, func) {
	this.add_handler(request + "_info", function(msg) {
		func(msg.value);
	}, true);
	this.send("request_" + request, value);
};
