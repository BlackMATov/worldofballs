// http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
(function() {
	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for ( var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x ) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame  = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}
	if ( !window.requestAnimationFrame ) {
		window.requestAnimationFrame = function(callback, element) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function(){
				callback(currTime + timeToCall);
			}, timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};
	}
	if ( !window.cancelAnimationFrame ) {
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
	}
}());

// http://javascript.ru/Math.random
function wob_random_int(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// ----------------------------------------------------------------------------
// 
// Defines
// 
// ----------------------------------------------------------------------------

var WOB_REQUEST_PING_INTERVAL   = 1000;
var WOB_SEND_MOUSE_POS_INTERVAL = 33;
var WOB_STRESS_TEST_MODE        = false;

// ----------------------------------------------------------------------------
// 
// Data holder
// 
// ----------------------------------------------------------------------------

var data_holder = {
	start_time             : -1,
	mouse_pos              : {x:0, y:0},
	mouse_downed           : false,
	auto_force_timeout     : null,
	request_ping_timeout   : null,
	send_mouse_pos_timeout : null,
	client                 : new Client(),
	entities               : []
};

// ----------------------------------------------------------------------------
// 
// Start
// 
// ----------------------------------------------------------------------------

$(function(){
	data_holder.start_time = Date.now();
	data_holder.client.start(game_start, game_finish);
	data_holder.client.add_handler("world_info", world_info_handler);
});

var game_start = function() {
	console.log("online");
	requestAnimationFrame(game_step);
	data_holder.request_ping_timeout   = setTimeout(request_ping_action,   WOB_REQUEST_PING_INTERVAL);
	data_holder.send_mouse_pos_timeout = setTimeout(send_mouse_pos_action, WOB_SEND_MOUSE_POS_INTERVAL);
	if ( WOB_STRESS_TEST_MODE ) {
		data_holder.auto_force_timeout = setTimeout(auto_force_action, WOB_SEND_MOUSE_POS_INTERVAL);
	}
	$("#map_canvas").mouseup(function(e){
		if ( e.which == 1 ) {
			var x = e.pageX - $("#map_canvas").offset().left;
			var y = e.pageY - $("#map_canvas").offset().top;
			mouse_up_action(x, y);
		}
	});
	$("#map_canvas").mousedown(function(e){
		if ( e.which == 1 ) {
			var x = e.pageX - $("#map_canvas").offset().left;
			var y = e.pageY - $("#map_canvas").offset().top;
			mouse_down_action(x, y);
		}
	});
	$("#map_canvas").mousemove(function(e){
		if ( e.which == 1 ) {
			var x = e.pageX - $("#map_canvas").offset().left;
			var y = e.pageY - $("#map_canvas").offset().top;
			mouse_move_action(x, y);
		}
	});
};

var game_finish = function() {
	console.log("offline");
	if ( data_holder.request_ping_timeout ) {
		clearTimeout(data_holder.request_ping_timeout);
		data_holder.request_ping_timeout = null;
	}
	if ( data_holder.send_mouse_pos_timeout ) {
		clearTimeout(data_holder.send_mouse_pos_timeout);
		data_holder.send_mouse_pos_timeout = null;
	}
};

var game_step = function() {
	draw_map();
	requestAnimationFrame(game_step);
};

// ----------------------------------------------------------------------------
// 
// Actions
// 
// ----------------------------------------------------------------------------

var auto_force_action = function() {
	mouse_down_action(wob_random_int(0,1024), wob_random_int(0,1024));
	data_holder.auto_force_timeout = setTimeout(
		auto_force_action, WOB_SEND_MOUSE_POS_INTERVAL);
};

var request_ping_action = function() {
	var client     = data_holder.client;
	var start_time = data_holder.start_time;
	if ( start_time >= 0 ) {
		client.request("ping", {
			time : Date.now() - start_time
		}, function(value) {
			var ping = Date.now() - start_time - value.time;
			console.log(
				"ping:", ping,
				"sps:",  client.get_sps(),
				"msp:",  client.get_mps());
		});
	}
	data_holder.request_ping_timeout = setTimeout(
		request_ping_action, WOB_REQUEST_PING_INTERVAL);
};

var send_mouse_pos_action = function() {
	if ( data_holder.mouse_downed ) {
		force_action();
	}
	data_holder.send_mouse_pos_timeout = setTimeout(
		send_mouse_pos_action, WOB_SEND_MOUSE_POS_INTERVAL);
};

var force_action = function() {
	var client    = data_holder.client;
	var mouse_pos = data_holder.mouse_pos;
	client.send("force", mouse_pos);
};

var unforce_action = function() {
	var client  = data_holder.client;
	client.send("unforce");
};

var mouse_up_action = function(x, y) {
	data_holder.mouse_downed = false;
	mouse_move_action(x, y);
	unforce_action();
};

var mouse_down_action = function(x, y) {
	data_holder.mouse_downed = true;
	mouse_move_action(x, y);
	force_action();
};

var mouse_move_action = function(x, y) {
	data_holder.mouse_pos = {x:x, y:y};
};

// ----------------------------------------------------------------------------
// 
// Handlers
// 
// ----------------------------------------------------------------------------

var world_info_handler = function(msg) {
	var entities = [];
	var value = msg.value;
	if ( value.entities ) {
		for ( var i = 0; i < value.entities.length; ++i ) {
			var entity = value.entities[i];
			entities.push({
				x : entity.x,
				y : entity.y,
				t : entity.t,
				r : entity.r
			});
		}
	}
	data_holder.entities = entities;
};

// ----------------------------------------------------------------------------
// 
// Helpers
// 
// ----------------------------------------------------------------------------

var draw_map = function() {
	var canvas = document.getElementById('map_canvas');
	if ( canvas.getContext ) {
		var ctx = canvas.getContext('2d');
		canvas.width  = window.innerWidth;
		canvas.height = window.innerHeight;
		ctx.fillStyle = "black";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		draw_entities(ctx);
	}
};

var draw_entities = function(ctx) {
	var entities = data_holder.entities;
	for ( var i = 0; i < entities.length; ++i ) {
		var entity = entities[i];
		ctx.beginPath();
		ctx.arc(entity.x, entity.y, entity.r, 0, 2 * Math.PI, false);
		ctx.fillStyle = entity.t == 'user' ? 'green' : 'red';
		ctx.fill();
		ctx.lineWidth = 1;
		ctx.strokeStyle = '#003300';
		ctx.stroke();
	}
};
