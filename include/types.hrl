-define(WORLD_WIDTH,              1024.0).
-define(WORLD_HEIGHT,             768.0).
-define(WORLD_FOOD_COUNT,         120).
-define(WORLD_FOOD_MIN_SIZE,      10.0).
-define(WORLD_FOOD_MAX_SIZE,      40.0).
-define(WORLD_MIN_LIVE_SIZE,      5.0).
-define(WORLD_MAX_EMIT_SIZE,      30.0).
-define(WORLD_REFRESH_INTERVAL,   20).
-define(WORLD_BROADCAST_INTERVAL, 50).
-define(WORLD_RECREATE_INTERVAL,  40000).

-record(user_state2, {
	pid                  :: pid(),
	wspid                :: pid(),
	idir   = vec2:zero() :: vec2(),
	entity = -1          :: integer()
}).

-record(game_state, {
	world :: pid()
}).

-record(world_state, {
	pworld       = 0          :: integer(),
	entities     = dict:new() :: dict(),
	last_free_id = 0          :: integer()
}).

-record(food_brain, {
}).

-record(user_brain, {
	pid             :: pid(),
	lost_area = 0.0 :: float()
}).

-record(entity_state, {
	id    :: integer(),
	r     :: float(),
	pos   :: vec2(),
	body  :: integer(),
	brain :: brain_desc()
}).

-record(stats_state, {
}).

-record(loginer_state, {
	users = dict:new() :: dict()
}).

-type json          () :: list() | number().
-type vec2          () :: {float(), float()}.

-type user_state2   () :: #user_state2{}.
-type game_state    () :: #game_state{}.
-type world_state   () :: #world_state{}.
-type entity_state  () :: #entity_state{}.
-type stats_state   () :: #stats_state{}.
-type loginer_state () :: #loginer_state{}.

-type food_brain    () :: #food_brain{}.
-type user_brain    () :: #user_brain{}.

-type brain_type    () :: food | user.
-type brain_state   () :: food_brain() | user_brain().
-type brain_desc    () :: {brain_type(), brain_state()}.
