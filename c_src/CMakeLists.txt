cmake_minimum_required(VERSION 2.8)
project(wob_physics)
set(CMAKE_BUILD_TYPE Release)

if(MSVC)
	set(COMPILER_MSVC ON)
	message("COMPILER_MSVC")
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
	set(COMPILER_GCC ON)
	message("COMPILER_GCC")
else()
	message(FATAL_ERROR "COMPILER not detected")
endif()

file(GLOB src
	"*.cpp"
	"*.hpp"
	"Box2D/*.*"
	"Box2D/Collision/*.*"
	"Box2D/Collision/Shapes/*.*"
	"Box2D/Common/*.*"
	"Box2D/Dynamics/*.*"
	"Box2D/Dynamics/Contacts/*.*"
	"Box2D/Dynamics/Joints/*.*"
)

if(COMPILER_MSVC)
	add_definitions(-D_CRT_SECURE_NO_WARNINGS)
	set(CMAKE_C_FLAGS           "-DWIN32 -D_WINDOWS /EHsc")
	set(CMAKE_CXX_FLAGS         "-DWIN32 -D_WINDOWS /EHsc")
	set(CMAKE_C_FLAGS_DEBUG     "/Ob0 /Od /RTC1 /ZI /W4 /MTd")
	set(CMAKE_CXX_FLAGS_DEBUG   "/Ob0 /Od /RTC1 /ZI /W4 /MTd")
	set(CMAKE_C_FLAGS_RELEASE   "/Ob2 /O2 /W4 /MT")
	set(CMAKE_CXX_FLAGS_RELEASE "/Ob2 /O2 /W4 /MT")
endif()

set(ADDED_DEBUG_FLAGS       " -D_DEBUG -DNRELEASE")
set(ADDED_RELEASE_FLAGS     " -DNDEBUG -D_RELEASE")
set(CMAKE_C_FLAGS_DEBUG     ${CMAKE_C_FLAGS_DEBUG}${ADDED_DEBUG_FLAGS})
set(CMAKE_CXX_FLAGS_DEBUG   ${CMAKE_CXX_FLAGS_DEBUG}${ADDED_DEBUG_FLAGS})
set(CMAKE_C_FLAGS_RELEASE   ${CMAKE_C_FLAGS_RELEASE}${ADDED_RELEASE_FLAGS})
set(CMAKE_CXX_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE}${ADDED_RELEASE_FLAGS})

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories("d:/erl5.10.4/erts-5.10.4/include")
include_directories("d:/Programs/erl5.10.4/erts-5.10.4/include")

set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/../priv)
add_library(wob_physics SHARED ${src})
