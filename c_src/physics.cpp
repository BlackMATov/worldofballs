#include "common.hpp"
#include <Box2D/Box2D.h>

#ifdef _WIN32
#  pragma comment(lib, "winmm")
#  include <windows.h>
#endif

#if defined(__linux__) || defined(__APPLE__)
#  include <sys/time.h>
#endif

// ----------------------------------------------------------------------------
// 
// Defines
// 
// ----------------------------------------------------------------------------

const float s_world_scale  = 30.f;

struct body_t;
struct world_t;
struct listener_t;
struct contact_filter_t;

typedef std::pair<float, float> vec2_t;
typedef std::pair<int, int>     contact_t;
typedef std::vector<contact_t>  contacts_t;

// ----------------------------------------------------------------------------
// 
// Helpers
// 
// ----------------------------------------------------------------------------

#ifdef _WIN32
static unsigned system_ticks() {
	return (unsigned)::timeGetTime();
}
#endif

#if defined(__linux__) || defined(__APPLE__)
static unsigned system_ticks() {
	timeval now;
	gettimeofday(&now, NULL);
	return (now.tv_sec ) * 1000 + (now.tv_usec) / 1000;
}
#endif

vec2_t make_vec2(float x, float y) {
	return std::make_pair(x, y);
}

contact_t make_contact(int a, int b) {
	return std::make_pair(a, b);
}

float game_to_world(float v) {
	return v / s_world_scale;
}

vec2_t game_to_world(const vec2_t& v) {
	return make_vec2(
		game_to_world(v.first),
		game_to_world(v.second));
}

float world_to_game(float v) {
	return v * s_world_scale;
}

vec2_t world_to_game(const vec2_t& v) {
	return make_vec2(
		world_to_game(v.first),
		world_to_game(v.second));
}

// ----------------------------------------------------------------------------
// 
// Structs
// 
// ----------------------------------------------------------------------------

enum body_type_e {
	body_box,
	body_circle
};

struct body_t {
	int         id;
	b2Body*     body;
	world_t*    world;
	body_type_e type;
};

struct world_t {
	b2World*          world;
	listener_t*       listener;
	contact_filter_t* contact_filter;
	size_t            last_update;
	contacts_t        contacts;
};

struct listener_t : b2ContactListener {
	void PreSolve(b2Contact* contact, const b2Manifold*) {
		if ( contact->IsTouching() && contact->IsEnabled() ) {
			b2Body* body_a      = contact->GetFixtureA()->GetBody();
			b2Body* body_b      = contact->GetFixtureB()->GetBody();
			body_t* body_a_data = static_cast<body_t*>(body_a->GetUserData());
			body_t* body_b_data = static_cast<body_t*>(body_b->GetUserData());
			if ( body_a_data && body_b_data ) {
				world_t* world = body_a_data->world;
				if ( body_a_data->type == body_circle && body_b_data->type == body_circle ) {
					contact->SetEnabled(false);
					contact_t contact = make_contact(body_a_data->id, body_b_data->id);
					world->contacts.push_back(contact);
				}
			}
		}
	}
	void BeginContact(b2Contact* contact) {
		(void)contact;
	}
	void EndContact(b2Contact* contact) {
		(void)contact;
	}
};

struct contact_filter_t : b2ContactFilter {
	bool ShouldCollide(b2Fixture* fixture_a, b2Fixture* fixture_b) {
		return b2ContactFilter::ShouldCollide(fixture_a, fixture_b);
	}
};

// ----------------------------------------------------------------------------
// 
// Real functions
// 
// ----------------------------------------------------------------------------

world_t* create_world() {
	world_t* world        = new world_t();
	world->world          = new b2World(b2Vec2(0.f, 0.f));
	world->listener       = new listener_t();
	world->contact_filter = new contact_filter_t();
	world->last_update    = system_ticks();
	world->world->SetContactListener(world->listener);
	world->world->SetContactFilter(world->contact_filter);
	world->world->SetAutoClearForces(false);
	return world;
}

void update_world(world_t* world) {
	unsigned count = 0;
	const unsigned max_count = 5;
	const unsigned step_ms = 20;
	const unsigned now_ticks = system_ticks();
	world->contacts.clear();
	while ( world->last_update < now_ticks && count++ < max_count ) {
		world->last_update += step_ms;
		world->world->Step(1.f / (1000.f / static_cast<float>(step_ms)), 6, 6);
	}
	world->world->ClearForces();
}

std::vector<contact_t> contacts_world(world_t* world) {
	/*
	std::vector<contact_t> contacts;
	for ( b2Contact* contact = world->world->GetContactList();
		contact; contact = contact->GetNext() )
	{
		if ( contact->IsTouching() ) {
			b2Body* body_a      = contact->GetFixtureA()->GetBody();
			b2Body* body_b      = contact->GetFixtureB()->GetBody();
			body_t* body_a_data = static_cast<body_t*>(body_a->GetUserData());
			body_t* body_b_data = static_cast<body_t*>(body_b->GetUserData());
			if ( body_a_data && body_b_data ) {
				contacts.push_back(
					make_contact(body_a_data->id, body_b_data->id));
			}
		}
	}
	return contacts;*/
	return world->contacts;
}

unsigned destroy_world(world_t* world) {
	delete world->listener;
	delete world->world;
	delete world;
	return 0;
}

body_t* create_circle(world_t* world, const vec2_t& pos, float r, int id) {
	body_t* body = new body_t();
	body->id = id;
	body->type = body_circle;
	body->world = world;
	
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(
		game_to_world(pos.first),
		game_to_world(pos.second));
	body->body = world->world->CreateBody(&bodyDef);
	
	b2CircleShape shape;
	shape.m_radius = game_to_world(r);
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.5f;
	body->body->CreateFixture(&fixtureDef);
	
	body->body->SetUserData(body);
	return body;
}

body_t* create_box(world_t* world, const vec2_t& pos, const vec2_t& size, int id) {
	body_t* body = new body_t();
	body->id = id;
	body->type = body_box;
	body->world = world;
	
	b2BodyDef bodyDef;
	bodyDef.position.Set(
		game_to_world(pos.first),
		game_to_world(pos.second));
	body->body = world->world->CreateBody(&bodyDef);
	
	b2PolygonShape shape;
	shape.SetAsBox(
		game_to_world(size.first),
		game_to_world(size.second));
	
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	body->body->CreateFixture(&fixtureDef);
	
	body->body->SetUserData(body);
	return body;
}

unsigned destroy_body(body_t* body) {
	body->body->GetWorld()->DestroyBody(body->body);
	delete body;
	return 0;
}

body_t* replace_circle(body_t* body, float r) {
	int           id    = body->id;
	world_t*      world = body->world;
	const b2Vec2  pos   = body->body->GetPosition();
	const b2Vec2  lvel  = body->body->GetLinearVelocity();
	const float   avel  = body->body->GetAngularVelocity();
	destroy_body(body);
	body_t* new_body = create_circle(world, world_to_game(make_vec2(pos.x, pos.y)), r, id);
	new_body->body->SetLinearVelocity(lvel);
	new_body->body->SetAngularVelocity(avel);
	return new_body;
}

void apply_force(body_t* body, const vec2_t& force) {
	body->body->ApplyForce(
		b2Vec2(force.first, force.second),
		body->body->GetPosition(),
		true);
}

void apply_impulse(body_t* body, const vec2_t& impulse) {
	body->body->ApplyLinearImpulse(
		b2Vec2(impulse.first, impulse.second),
		body->body->GetPosition(),
		true);
}

int get_body_id(body_t* body) {
	return body->id;
}

vec2_t get_body_pos(body_t* body) {
	const b2Vec2& pos = body->body->GetPosition();
	return world_to_game(make_vec2(pos.x, pos.y));
}

vec2_t get_body_lvel(body_t* body) {
	const b2Vec2& vel = body->body->GetLinearVelocity();
	return world_to_game(make_vec2(vel.x, vel.y));
}

void set_body_lvel(body_t* body, const vec2_t& lvel) {
	body->body->SetLinearVelocity(b2Vec2(lvel.first, lvel.second));
}

world_t* get_body_world(body_t* body) {
	return body->world;
}

// ----------------------------------------------------------------------------
// 
// Glue functions
// 
// ----------------------------------------------------------------------------

static ERL_NIF_TERM create_world_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM[])
{
	if ( argc != 0 ) return enif_make_badarg(env);
	world_t* world = create_world();
	return nif_put_arg(env, world);
}

static ERL_NIF_TERM update_world_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	world_t* world = NULL;
	if ( argc != 1 ) return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &world)) return enif_make_badarg(env);
	update_world(world);
	return nif_put_arg(env, world);
}

static ERL_NIF_TERM contacts_world_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	world_t* world = NULL;
	if ( argc != 1 ) return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &world)) return enif_make_badarg(env);
	std::vector<contact_t> contacts = contacts_world(world);
	return nif_put_arg(env, contacts);
}

static ERL_NIF_TERM destroy_world_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	world_t* world = NULL;
	if ( argc != 1 )                         return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &world)) return enif_make_badarg(env);
	unsigned ret = destroy_world(world);
	return nif_put_arg(env, ret);
}

static ERL_NIF_TERM create_box_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	world_t* world = NULL;
	vec2_t pos, size;
	int id = 0;
	if ( argc != 4 )                         return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &world)) return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[1], &pos))   return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[2], &size))  return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[3], &id))    return enif_make_badarg(env);
	body_t* body = create_box(world, pos, size, id);
	return nif_put_arg(env, body);
}

static ERL_NIF_TERM create_circle_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	world_t* world = NULL;
	vec2_t pos;
	float r = 0.f;
	int id = 0;
	if ( argc != 4 )                         return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &world)) return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[1], &pos))   return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[2], &r))     return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[3], &id))    return enif_make_badarg(env);
	body_t* body = create_circle(world, pos, r, id);
	return nif_put_arg(env, body);
}

static ERL_NIF_TERM destroy_body_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	if ( argc != 1 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	unsigned ret = destroy_body(body);
	return nif_put_arg(env, ret);
}

static ERL_NIF_TERM replace_circle_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	float r = 0.f;
	if ( argc != 2 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[1], &r))    return enif_make_badarg(env);
	body_t* new_body = replace_circle(body, r);
	return nif_put_arg(env, new_body);
}

static ERL_NIF_TERM apply_force_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	vec2_t force;
	if ( argc != 2 )                         return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body))  return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[1], &force)) return enif_make_badarg(env);
	apply_force(body, force);
	return nif_put_arg(env, body);
}

static ERL_NIF_TERM apply_impulse_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	vec2_t impulse;
	if ( argc != 2 )                           return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body))    return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[1], &impulse)) return enif_make_badarg(env);
	apply_impulse(body, impulse);
	return nif_put_arg(env, body);
}

static ERL_NIF_TERM get_body_id_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	if ( argc != 1 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	return nif_put_arg(env, get_body_id(body));
}

static ERL_NIF_TERM get_body_pos_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	if ( argc != 1 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	return nif_put_arg(env, get_body_pos(body));
}

static ERL_NIF_TERM get_body_lvel_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	if ( argc != 1 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	return nif_put_arg(env, get_body_lvel(body));
}

static ERL_NIF_TERM set_body_lvel_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	vec2_t lvel;
	if ( argc != 2 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[1], &lvel)) return enif_make_badarg(env);
	set_body_lvel(body, lvel);
	return nif_put_arg(env, body);
}

static ERL_NIF_TERM get_body_world_nif(
	ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
	body_t* body = NULL;
	if ( argc != 1 )                        return enif_make_badarg(env);
	if ( !nif_get_arg(env, argv[0], &body)) return enif_make_badarg(env);
	return nif_put_arg(env, get_body_world(body));
}

// ----------------------------------------------------------------------------
// 
// Export
// 
// ----------------------------------------------------------------------------

static ErlNifFunc nif_funcs[] = {
	{"create_world",   0, create_world_nif},
	{"update_world",   1, update_world_nif},
	{"contacts_world", 1, contacts_world_nif},
	{"destroy_world",  1, destroy_world_nif},
	{"create_box",     4, create_box_nif},
	{"create_circle",  4, create_circle_nif},
	{"destroy_body",   1, destroy_body_nif},
	{"replace_circle", 2, replace_circle_nif},
	{"apply_force",    2, apply_force_nif},
	{"apply_impulse",  2, apply_impulse_nif},
	{"get_body_id",    1, get_body_id_nif},
	{"get_body_pos",   1, get_body_pos_nif},
	{"get_body_lvel",  1, get_body_lvel_nif},
	{"set_body_lvel",  2, set_body_lvel_nif},
	{"get_body_world", 1, get_body_world_nif}
};

ERL_NIF_INIT(wob_physics, nif_funcs, NULL, NULL, NULL, NULL)
