#pragma once

#include <vector>
#include <utility>
#include <cassert>
#include <erl_nif.h>

// ----------------------------------------------------------------------------
// 
// nif_get_arg nif glue
// 
// ----------------------------------------------------------------------------

bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, int* out_arg) {
	return 0 != enif_get_int(env, term, out_arg);
}

bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, long* out_arg) {
	return 0 != enif_get_long(env, term, out_arg);
}

bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, unsigned int* out_arg) {
	return 0 != enif_get_uint(env, term, out_arg);
}

bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, unsigned long* out_arg) {
	return 0 != enif_get_ulong(env, term, out_arg);
}

bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, float* out_arg) {
	double tmp = 0.0;
	if ( !enif_get_double(env, term, &tmp) ) {
		return false;
	}
	*out_arg = static_cast<float>(tmp);
	return true;
}

bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, double* out_arg) {
	return 0 != enif_get_double(env, term, out_arg);
}

template < typename T >
bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, T** out_arg) {
	unsigned long raw_ptr = 0;
	if ( !enif_get_ulong(env, term, &raw_ptr) ) {
		return false;
	}
	assert(sizeof(*out_arg) <= sizeof(unsigned long));
	*out_arg = reinterpret_cast<T*>(raw_ptr);
	return true;
}

template < typename T1, typename T2 >
bool nif_get_arg(ErlNifEnv* env, ERL_NIF_TERM term, std::pair<T1, T2>* out_arg) {
	int count = 0;
	const ERL_NIF_TERM* tuple = NULL;
	if ( !enif_get_tuple(env, term, &count, &tuple) || count != 2 ) {
		return false;
	}
	T1 v1; T2 v2;
	if ( !nif_get_arg(env, tuple[0], &v1) || !nif_get_arg(env, tuple[1], &v2) ) {
		return false;
	}
	*out_arg = std::make_pair(v1, v2);
	return true;
}

// ----------------------------------------------------------------------------
// 
// nif_put_arg and nif_put_ptr nif glue
// 
// ----------------------------------------------------------------------------

ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, int arg) {
	return enif_make_int(env, arg);
}

ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, long arg) {
	return enif_make_long(env, arg);
}

ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, unsigned int arg) {
	return enif_make_uint(env, arg);
}

ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, unsigned long arg) {
	return enif_make_ulong(env, arg);
}

ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, float arg) {
	return enif_make_double(env, static_cast<double>(arg));
}

ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, double arg) {
	return enif_make_double(env, arg);
}

template < typename T >
ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, const T* arg) {
	assert(sizeof(arg) <= sizeof(unsigned long));
	return enif_make_ulong(env, reinterpret_cast<unsigned long>(arg));
}

template < typename T1, typename T2 >
ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, const std::pair<T1, T2>& pair) {
	return enif_make_tuple(env, 2,
		nif_put_arg(env, pair.first),
		nif_put_arg(env, pair.second));
}

template < typename T >
ERL_NIF_TERM nif_put_arg(ErlNifEnv* env, const std::vector<T>& list) {
	ERL_NIF_TERM erl_list = enif_make_list(env, 0);
	for ( typename std::vector<T>::const_iterator
		iter = list.begin(), end = list.end();
		iter != end; ++iter )
	{
		erl_list = enif_make_list_cell(
			env, nif_put_arg(env, *iter), erl_list);
	}
	return erl_list;
}
